ublic class Empresa {
    private String nombre;
    private String direccion;
    private String ciudad;
    private String codigoPostal;
    private String telefono;
    private String estado;
    private boolean subsidiario;

    public Empresa(String nombre, String direccion, String ciudad, String codigoPostal, String telefono, String estado, boolean subsidiario) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.codigoPostal = codigoPostal;
        this.telefono = telefono;
        this.estado = estado;
        this.subsidiario = subsidiario;
    }

    // Getters y Setters para todos los atributos

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public boolean isSubsidiario() {
        return subsidiario;
    }

    public void setSubsidiario(boolean subsidiario) {
        this.subsidiario = subsidiario;
    }

    // Otros métodos de la clase Empresa si es necesario
}