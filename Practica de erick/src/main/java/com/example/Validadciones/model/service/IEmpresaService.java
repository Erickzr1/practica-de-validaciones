package com.example.Validadciones.model.service;

import com.example.Validadciones.model.entity.Empresa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IEmpresaService {

    public Empresa findById(Long id);
    public Empresa findByNombreAndRuc(String id);

    public List<Empresa> findAll();

    public Page<Empresa> findALl(Pageable pageable);

    public Empresa save(Empresa empresa);

    public void delete();

}
