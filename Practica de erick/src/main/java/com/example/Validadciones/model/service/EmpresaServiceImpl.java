package com.example.Validadciones.model.service;

import com.example.Validadciones.model.dao.IEmpresaDao;
import com.example.Validadciones.model.entity.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EmpresaServiceImpl implements IEmpresaService{

    @Autowired
    private IEmpresaDao empresaDao;

    @Override
    public Empresa findById(Long id) {
        return null;
    }

    @Override
    public Empresa findByNombreAndRuc(String id) {
        return null;
    }

    @Override
    public List<Empresa> findAll() {
        return null;
    }

    @Override
    public Page<Empresa> findALl(Pageable pageable) {
        return empresaDao.findAll(pageable);
    }

    @Override
    public Empresa save(Empresa empresa) {

        //throw new IllegalArgumentException("Esta es una excepcion Controlada");
        return empresaDao.save(empresa);
    }

    @Override
    public void delete() {

    }
}
